<?php

// 1. Créer une variable $prenom qui contient ton prénom, puis afficher son contenu.

$prenom = "Jean-Eudes";

echo $prenom;

echo '<br><br>';

// 2. Reprendre cette variable et grâce à elle, afficher la phrase "Je m'appelle XXXX"
// (où XXXX est le contenu de la variable $prenom).

echo "Je m'appelle ". $prenom;

echo '<br><br>';

// Ou :
echo "Je m'appelle $prenom";

echo '<br><br>';

// 3. Créer une variable $age qui contient ton âge, puis afficher son contenu.

$age = 33;

echo $age;

echo '<br><br>';

// 4. Reprendre les variables $prenom et $age pour afficher "Je m'appelle XXXX et j'ai Y ans"
// (où XXXX est le contenu de la variable $prenom et Y celui de la variable $age).

echo "Je m'appelle " . $prenom . " et j'ai " . $age . " ans";

echo '<br><br>';

// Ou :
echo "Je m'appelle $prenom et j'ai $age ans";
