<?php
// 1. Créer une variable $aliment qui contient le mot "fraise"

$aliment = "fraise";


// 2. Ecrire la condition qui permet de vérifier le contenu de la variable $aliment.
// > Si elle contient "fraise", afficher "C'est un fruit !",
// > sinon afficher "Je ne connais pas cet aliment."

if ($aliment == "fraise") {
    echo "C'est un fruit !";
} else {
    echo "Je ne connais pas cet aliment.";
}
echo "<br><br>";


// 3. Modifier la condition écrite pour qu'elle gère en plus le cas où $aliment contient "carotte",
// et afficher "C'est un légume" dans ce cas.

if ($aliment == "fraise") {
    echo "C'est un fruit !";
} elseif ($aliment == "carotte") {
    echo "C'est un légume";
} else {
    echo "Je ne connais pas cet aliment.";
}

echo "<br><br>";


// 4. Modifier la condition pour afficher "C'est un fruit" si aliment contient "fraise" OU "framboise" OU "cerise".

if ($aliment == "fraise" || $aliment = "framboise" || $aliment = "cerise") {
    echo "C'est un fruit !";
} elseif ($aliment == "carotte") {
    echo "C'est un légume";
} else {
    echo "Je ne connais pas cet aliment.";
}

echo "<br><br>";


// Cas concret :
/* 
5. Pour la nouvelle radio libre de Nantes, nous avons une interface d'administration à 
développer afin que certaines personnes puissent gérer le contenu affiché sur leur site web.

Chaque utilisateur n'a pas forcément les mêmes droits d'accès, pour gérer ces droits chaque utilisateur a un ou
plusieurs rôles.

Voici les rôles :
- Admin
- Editor
- Author

Voici le menu à afficher pour les Admin :
- Articles
- Médias
- Pages
- Extensions
- Utilisateurs
- Paramètres

Le menu à afficher pour les Editor :
- Articles
- Médias
- Pages

Le menu pour les Author :
- Articles

Si aucun rôle n'est affecté à l'utilisateur ou s'il ne fait pas partie d'un de ces 3 rôles, afficher :
"Vous n'avez pas les droits !".

A faire :

a) Initialiser une variable "$userRole" avec l'un des 3 rôles.

b) Afficher les liens de menus (écrire simplement le nom de chaque lien les uns sous les autres) en fonction du rôle
de l'utilisateur courant.

c) Optimiser l'algorithme pour que vous n'ayez dans le code qu'une seule instruction "echo" par lien de menu.
Par exemple il ne faut pas avoir cela :

if (ma condition 1) {
    echo "Articles";
    ...
} elseif (ma condition 2) {
    echo "Articles";
    ...
}

d) Exécuter le code avec chaque rôle pour voir si le menu s'affiche correctement pour chaque rôle.
*/


$userRole = "Editor";

if ($userRole != "Author" && $userRole != "Editor" && $userRole != "Admin") {
    echo "Vous n'avez pas les droits !";
} else {
    echo "Articles";
    echo "<br>";
    
    if ($userRole == "Editor" || $userRole == "Admin") {
        echo "Médias";
        echo "<br>";
        echo "Pages";
        echo "<br>";
    }
    
    if ($userRole == "Admin") {
        echo "Extensions";
        echo "<br>";
        echo "Utilisateurs";
        echo "<br>";
        echo "Paramètres";
        echo "<br>";
    }
}
