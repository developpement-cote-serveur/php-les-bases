<?php

// 1. Paul et Virginie vont aux champignons avec un panier chacun⋅e.
// On peut modéliser ces paniers à l'aide de deux variables : $panierPaul et $panierVirginie.
// Comme c'est une bonne année, ils en ont ramassé respectivement 37 et 42.
//
// > Créer ces variables avec leurs valeurs adéquates.

// TON CODE ICI


// 2. En rentrant à la maison, ils ont mis leurs paniers en commun dans une variable appelée $panierCommun.
//
// > Remplir cette variable avec le cumul des deux paniers.

// TON CODE ICI


// 3. Dans une variable appelée $moyenne, faire la moyenne du nombre de champignons ramassés par personne (somme totale des champis divisée par le nombre de paniers).

// TON CODE ICI

