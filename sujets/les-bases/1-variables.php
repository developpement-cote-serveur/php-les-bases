<?php

// 1. Créer une variable $prenom qui contient ton prénom, puis afficher son contenu.

// TON CODE ICI


// 2. Reprendre cette variable et grâce à elle, afficher la phrase "Je m'appelle XXXX"
// (où XXXX est le contenu de la variable $prenom).

// TON CODE ICI


// 3. Créer une variable $age qui contient ton âge, puis afficher son contenu.

// TON CODE ICI


// 4. Reprendre les variables $prenom et $age pour afficher "Je m'appelle XXXX et j'ai Y ans"
// (où XXXX est le contenu de la variable $prenom et Y celui de la variable $age).

// TON CODE ICI


